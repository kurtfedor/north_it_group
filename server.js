const express        = require('express');
const MongoClient    = require('mongodb').MongoClient;
const bodyParser     = require('body-parser');
const url            = require('./env.json').mongodb
const app            = express();
const port = 8000;
app.use(bodyParser.json());
MongoClient.connect(url, (err, db) => {
    var database=db.db('Cluster0');

    if (err) return console.log(err)
    require('./app/routes')(app, database);

    app.listen(port, () => {
        console.log('We are live on ' + port);
    });
})