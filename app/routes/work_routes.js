module.exports = function (app, db) {

    app.put('/works/:id', (req, res) => {
        const id = req.params.id;
        const query = {id: id};
        const work = {
            id: req.body.id,
            title: req.body.title,
            description: req.body.description,
            published: req.body.published,
            inventory: req.body.inventory,
            tags: req.body.tags,
            images: req.body.images
        };
        db.collection('works').update(query, work, (err, result) => {
            if (err) {
                res.send({'error': 'An error has occurred'});
            } else {
                res.send(work);
            }
        });
    });

    app.get('/works', (req, res) => {
        const options = {
            projection: {_id: 0},
        };
        const books = db.collection('works').find({}, options).toArray(function (err, result) {
            if (err) {
                res.send({'error': 'An error has occurred'});
            } else {
                res.send(result);
            }
            }
        );
    });

    app.get('/works/:id', (req, res) => {
        const id = req.params.id;
        const query = {id: id};
        const options = {
            projection: {_id: 0},
        };
        db.collection('works').findOne(query, options, (err, item) => {
            if (err) {
                res.send({'error': 'An error has occurred'});
            } else {
                res.send(item);
            }
        });
    });

    app.delete('/works/:id', (req, res) => {
        const id = req.params.id;
        const query = {id: id};
        db.collection('works').remove(query, (err, item) => {
            if (err) {
                res.send({'error': 'An error has occurred'});
            } else {
                res.send('Note ' + id + ' deleted!');
            }
        });
    });

    app.post('/works', (req, res) => {
        console.log(req.body);
        const work = {
            id: req.body.id,
            title: req.body.title,
            description: req.body.description,
            published: req.body.published,
            inventory: req.body.inventory,
            tags: req.body.tags,
            images: req.body.images
        };
        var collections = db.collection('works');
        collections.insertOne(work, (err, result) => {
            if (err) {
                res.send({'error': 'An error has occurred'});
            } else {
                res.send(result.ops[0]);
            }
        });
    });
};